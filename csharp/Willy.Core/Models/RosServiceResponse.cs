﻿using System.Collections.Generic;

namespace Willy.Core.Models
{
    public class RosServiceResponse
    {
        public string Name { get; set; }
        public List<object> Values { get; set; }
    }
}